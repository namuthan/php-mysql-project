<?php require_once("../includes/session.php"); ?>
<!-- //include the required data base connection file. -->
<?php require_once("../includes/db_connection.php"); ?>
<!-- //include the required functions. -->
<?php require_once("../includes/functions.php"); ?>
<!--include the require layout files -->
<?php include("../includes/layouts/header.php"); ?>
<?php find_selected_page();?>




<div id="main">
	<div id="navigation">
		<?php echo public_navigation($current_subject, $current_page)?><br />
	</div>
	
	<div id="page">
		<?php echo message(); ?>
		<?php if ($current_subject){ ?>
			<h2>Manage Subject</h2>
			Menu Name: <?php echo htmlentities($current_subject["menu_name"]); ?><br />
			

		<?php } elseif ($current_page) { ?>
			<?php echo htmlentities($current_page["content"]); ?>
	
		<?php } else { ?>
			Please select a subject or a page.
		<?php } ?>
	</div>
 </div>

		 




 <?php include("../includes/layouts/footer.php"); ?>

 
